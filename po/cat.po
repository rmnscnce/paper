# Language cat translations for io.posidon.Paper package.
# Copyright (C) 2022 THE io.posidon.Paper'S COPYRIGHT HOLDER
# This file is distributed under the same license as the io.posidon.Paper package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: io.posidon.Paper\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-23 09:28+0200\n"
"PO-Revision-Date: 2022-05-18 18:45+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: cat\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/app.desktop.in:8 src/ui/strings.vala:3
msgid "Paper"
msgstr "Paper"

#: data/app.desktop.in:9
msgid "Notes Manager"
msgstr "Administrador de Notas"

#: data/app.desktop.in:10
msgid "Take notes"
msgstr "Pren notes"

#: data/app.desktop.in:12
msgid "Notebook;Note;Notes;Text;Markdown;Notepad;Write;School;Post-it;Sticky;"
msgstr ""
"Lquadern;Nota;Notes;Text;Markdown;Notepad;Escriu;Cole;Col·legi;Post-it;"
"Sticky;"

#: data/app.desktop.in:21 src/ui/strings.vala:8 src/ui/shortcut_window.blp:17
#: src/ui/window.blp:43 src/ui/window.blp:155
msgid "New Note"
msgstr "Nota Nova"

#: data/app.desktop.in:25 src/ui/strings.vala:14 src/ui/shortcut_window.blp:32
#: src/ui/window.blp:180
msgid "New Notebook"
msgstr "Quadern Nou"

#: data/app.desktop.in:29 src/ui/strings.vala:39
#: src/ui/edit_view/edit_view.blp:113 src/ui/window.blp:128
msgid "Markdown Cheatsheet"
msgstr "Chuleta de Markdown"

#: data/app.desktop.in:33 src/ui/shortcut_window.blp:57
msgid "Preferences"
msgstr "Preferencies"

#: data/app.gschema.xml:6
msgid "Font"
msgstr "Font"

#: data/app.gschema.xml:7 src/ui/preferences.blp:15
msgid "The font notes will be displayed in"
msgstr "La font en la qual es mostraran les notes"

#: data/app.gschema.xml:11 src/ui/preferences.blp:25
msgid "OLED Mode"
msgstr "Mode OLED"

#: data/app.gschema.xml:15
msgid "Notes Directory"
msgstr "Carpeta de notes"

#: data/app.gschema.xml:16
msgid "Where the notebooks are stored"
msgstr "On s'emmagatzemen els quaderns"

#: data/app.metainfo.xml.in:5
msgid "Note-taking app"
msgstr "App per prendre apunts"

#: data/app.metainfo.xml.in:7
msgid "Create notebooks, take notes in markdown"
msgstr "Crea quaderns, pren notes en markdown"

#: data/app.metainfo.xml.in:8
msgid "Features:"
msgstr "Funcionalitats:"

#: data/app.metainfo.xml.in:10
msgid "Almost WYSIWYG markdown rendering"
msgstr "Renderizatge casi perfecte de markdown"

#: data/app.metainfo.xml.in:11
msgid "Searchable through GNOME search"
msgstr "Cercable traves de cerca de GNOME"

#: data/app.metainfo.xml.in:12
msgid "Highlight &amp; Strikethrough text formatting"
msgstr "Format ratllat y subratllat"

#: data/app.metainfo.xml.in:13
msgid "App recoloring based on notebook color"
msgstr "Recoloració de la interfície, basada en el color del quadern"

#: data/app.metainfo.xml.in:14
msgid "Trash can"
msgstr "Paperera"

#: data/app.metainfo.xml.in:30
msgid "Markdown document"
msgstr "Document markdown"

#: src/ui/strings.vala:4
msgid "Trash"
msgstr "Paperera"

#: src/ui/strings.vala:5
#, c-format
msgid "%d Notes"
msgstr "%d Notes"

#: src/ui/strings.vala:6
msgid "Are you sure you want to delete everything in the trash?"
msgstr "De veritat vol eliminar-ho tot en la paperera?"

#: src/ui/strings.vala:7 src/ui/window.blp:50
msgid "Empty Trash"
msgstr "Buidar Paperera"

#: src/ui/strings.vala:9
msgid "Create/choose a notebook before creating a note"
msgstr "Creu/esculli un quadern abans de crear una nota"

#: src/ui/strings.vala:10
msgid "Please, select a note to edit"
msgstr "Seleccioni una nota per editar, si us plau"

#: src/ui/strings.vala:11
msgid "Please, select a note to delete"
msgstr "Seleccioni una nota per eliminar, si us plau"

#: src/ui/strings.vala:12
msgid "Export Note"
msgstr "Exportar Nota"

#: src/ui/strings.vala:13
msgid "Export"
msgstr "Exportar"

#: src/ui/strings.vala:15
msgid "Rename Note"
msgstr "Reanomenar Nota"

#: src/ui/strings.vala:16
msgid "Move to Notebook"
msgstr "Moure a Quadern"

#: src/ui/strings.vala:17
msgid "Move"
msgstr "Moure"

#: src/ui/strings.vala:18
#, c-format
msgid "Note “%s” already exists in notebook “%s”"
msgstr "Nota “%s” ja existeix al quadern “%s”"

#: src/ui/strings.vala:19
#, c-format
msgid "Are you sure you want to delete the note “%s”?"
msgstr "De veritat vol eliminar la nota “%s”?"

#: src/ui/strings.vala:20
msgid "Delete Note"
msgstr "Eliminar Nota"

#: src/ui/strings.vala:21
#, c-format
msgid "Are you sure you want to delete the notebook “%s”?"
msgstr "De veritat vol eliminar el quadern “%s”?"

#: src/ui/strings.vala:22
msgid "Delete Notebook"
msgstr "Eliminar Quadern"

#: src/ui/strings.vala:23 src/ui/shortcut_window.blp:37
msgid "Edit Notebook"
msgstr "Editar Quadern"

#: src/ui/strings.vala:24
msgid "Note name shouldn’t contain “.” or “/”"
msgstr "Noms de notes no han de contenir “.” o “/”"

#: src/ui/strings.vala:25
msgid "Note name shouldn’t be blank"
msgstr "Noms de notes no han de ser buits"

#: src/ui/strings.vala:26
#, c-format
msgid "Note “%s” already exists"
msgstr "Nota “%s” ja existeix"

#: src/ui/strings.vala:27
msgid "Couldn’t create note"
msgstr "Nota no es va poder crear"

#: src/ui/strings.vala:28
msgid "Couldn’t change note"
msgstr "Nota no es va poder cambiar"

#: src/ui/strings.vala:29
msgid "Couldn’t delete note"
msgstr "Nota no es va poder borrar"

#: src/ui/strings.vala:30
msgid "Couldn’t restore note"
msgstr "Nota no es va poder recuperar"

#: src/ui/strings.vala:31
#, c-format
msgid "Saved “%s” to “%s”"
msgstr "“%s” se guardó en “%s”"

#: src/ui/strings.vala:32
msgid "Unknown error"
msgstr "Error desconocido"

#: src/ui/strings.vala:33
msgid "Notebook name shouldn’t contain “.” or “/”"
msgstr "Noms de quaderns no han de contenir “.” o “/”"

#: src/ui/strings.vala:34
msgid "Notebook name shouldn’t be blank"
msgstr "Noms de quaderns no han de ser buits"

#: src/ui/strings.vala:35
#, c-format
msgid "Notebook “%s” already exists"
msgstr "Quadern “%s” ja existeix"

#: src/ui/strings.vala:36
msgid "Couldn’t create notebook"
msgstr "Quadern es va poder crear"

#: src/ui/strings.vala:37
msgid "Couldn’t change notebook"
msgstr "Quadern es va poder cambiar"

#: src/ui/strings.vala:38
msgid "Couldn’t delete notebook"
msgstr "Quadern es va poder borrar"

#: src/ui/strings.vala:40
msgid "Search"
msgstr "Cercar"

#: src/ui/strings.vala:41 src/ui/notebook_sidebar/note_menu.blp:36
msgid "Rename"
msgstr "Reanomenar"

#: src/ui/strings.vala:42
msgid "Couldn’t find an app to handle file uris"
msgstr "No es va encontrar la app per manejar uris"

#: src/ui/strings.vala:43
msgid "Apply"
msgstr "Aplicar"

#: src/ui/strings.vala:44
#: src/ui/popup/notebook_selection_popup/notebook_selection_popup.blp:34
msgid "Cancel"
msgstr "Cancel·lar"

#: src/ui/strings.vala:45
msgid "Pick where the notebooks will be stored"
msgstr "Escull on s'emmagatzemen els quaderns"

#: src/ui/strings.vala:46
msgid "All Notes"
msgstr "Totes les Notes"

#: src/ui/edit_view/edit_view.blp:50 src/ui/markdown/heading_popover.blp:48
msgid "Plain Text"
msgstr "Text Simple"

#: src/ui/edit_view/edit_view.blp:51 src/ui/markdown/heading_popover.blp:12
msgid "Heading 1"
msgstr "Encapçalat 1"

#: src/ui/edit_view/edit_view.blp:52 src/ui/markdown/heading_popover.blp:18
msgid "Heading 2"
msgstr "Encapçalat 2"

#: src/ui/edit_view/edit_view.blp:53 src/ui/markdown/heading_popover.blp:24
msgid "Heading 3"
msgstr "Encapçalat 3"

#: src/ui/edit_view/edit_view.blp:54 src/ui/markdown/heading_popover.blp:30
msgid "Heading 4"
msgstr "Encapçalat 4"

#: src/ui/edit_view/edit_view.blp:55 src/ui/markdown/heading_popover.blp:36
msgid "Heading 5"
msgstr "Encapçalat 5"

#: src/ui/edit_view/edit_view.blp:56 src/ui/markdown/heading_popover.blp:42
msgid "Heading 6"
msgstr "Encapçalat 6"

#: src/ui/edit_view/edit_view.blp:67 src/ui/shortcut_window.blp:67
msgid "Bold"
msgstr "Negreta"

#: src/ui/edit_view/edit_view.blp:73 src/ui/shortcut_window.blp:72
msgid "Italic"
msgstr "Itàlic"

#: src/ui/edit_view/edit_view.blp:79 src/ui/shortcut_window.blp:77
msgid "Strikethrough"
msgstr "Ratllat"

#: src/ui/edit_view/edit_view.blp:85 src/ui/shortcut_window.blp:82
msgid "Highlight"
msgstr "Subratllat"

#: src/ui/edit_view/edit_view.blp:95 src/ui/shortcut_window.blp:87
msgid "Insert Link"
msgstr "Inserir Enllaç"

#: src/ui/edit_view/edit_view.blp:101
msgid "Insert Code"
msgstr "Inserir Codi"

#: src/ui/notebooks_bar/notebook_create_popup.blp:54
msgid "Notebook Name"
msgstr "Nom del Quadern"

#: src/ui/notebooks_bar/notebook_create_popup.blp:72
msgid "First Characters"
msgstr "Primers Caràcters"

#: src/ui/notebooks_bar/notebook_create_popup.blp:73
msgid "Initials"
msgstr "Inicials"

#: src/ui/notebooks_bar/notebook_create_popup.blp:74
msgid "Initials: camelCase"
msgstr "Inicials: camelCase"

#: src/ui/notebooks_bar/notebook_create_popup.blp:75
msgid "Initials: snake_case"
msgstr "Inicials: snake_case"

#: src/ui/notebooks_bar/notebook_create_popup.blp:76
msgid "Icon"
msgstr "Icona"

#: src/ui/notebooks_bar/notebook_create_popup.blp:104
msgid "Notebook Color"
msgstr "Color del Quadern"

#: src/ui/notebooks_bar/notebook_create_popup.blp:114
msgid "Create Notebook"
msgstr "Crear Quadern"

#: src/ui/notebooks_bar/notebook_menu.blp:16
msgid "Edit"
msgstr "Editar"

#: src/ui/notebooks_bar/notebook_menu.blp:26
msgid "Move all to Trash"
msgstr "Moure tot a la Paperera"

#: src/ui/notebooks_bar/notebooks_bar.blp:73
msgid "_New Notebook"
msgstr "Quadern _Nou"

#: src/ui/notebooks_bar/notebooks_bar.blp:77
msgid "_Edit Notebook"
msgstr "_Editar Quadern"

#: src/ui/notebooks_bar/notebooks_bar.blp:83
msgid "_Preferences"
msgstr "_Preferències"

#: src/ui/notebooks_bar/notebooks_bar.blp:87
msgid "_Keyboard Shortcuts"
msgstr "_Dreceres"

#: src/ui/notebooks_bar/notebooks_bar.blp:91
msgid "_About"
msgstr "_Sobre Paper"

#: src/ui/notebook_sidebar/note_create_popup.blp:37
msgid "Note Name"
msgstr "Nom de Nota"

#: src/ui/notebook_sidebar/note_create_popup.blp:46
msgid "Create Note"
msgstr "Crear Nota"

#: src/ui/notebook_sidebar/note_menu.blp:16
msgid "Restore from Trash"
msgstr "Recuperar de la Paperera"

#: src/ui/notebook_sidebar/note_menu.blp:26
msgid "Delete from Trash"
msgstr "Eliminar de la Paperera"

#: src/ui/notebook_sidebar/note_menu.blp:46
msgid "Move to Notebook…"
msgstr "Moure a Quadern…"

#: src/ui/notebook_sidebar/note_menu.blp:56
msgid "Move to Trash"
msgstr "Moure a la Paperera"

#: src/ui/notebook_sidebar/note_menu.blp:66
msgid "Open Containing Folder"
msgstr "Veure en Carpeta"

#: src/ui/popup/notebook_selection_popup/notebook_selection_popup.blp:39
msgid "Confirm"
msgstr "Confirmar"

#: src/ui/preferences.blp:14
msgid "Note Font"
msgstr "Font de Notas"

#: src/ui/preferences.blp:18
msgid "Pick a font for displaying the notes' content"
msgstr "Escull una font per mostrar el contingut de les notes"

#: src/ui/preferences.blp:26
msgid "Makes the dark theme pitch black"
msgstr "Fa el tema fosc de color negre"

#: src/ui/preferences.blp:36
msgid "Notes Storage Location"
msgstr "Emmagatzematge de Notes"

#: src/ui/preferences.blp:37
msgid "Where the notebooks are stored (requires app restart)"
msgstr "On s'emmagatzemen els quaderns (requereix reiniciar la app)"

#: src/ui/shortcut_window.blp:14
msgid "Note"
msgstr "Nota"

#: src/ui/shortcut_window.blp:22
msgid "Edit Note"
msgstr "Editar Nota"

#: src/ui/shortcut_window.blp:29
msgid "Notebook"
msgstr "Quadern"

#: src/ui/shortcut_window.blp:44
msgid "General"
msgstr "General"

#: src/ui/shortcut_window.blp:47
msgid "Show Keyboard Shortcuts"
msgstr "Mostrar Dreceres de Teclat"

#: src/ui/shortcut_window.blp:52
msgid "Quit"
msgstr "Sortir"

#: src/ui/shortcut_window.blp:64
msgid "Formatting"
msgstr "Format"

#: src/ui/shortcut_window.blp:94
msgid "Navigation"
msgstr "Navegació"

#: src/ui/shortcut_window.blp:97 src/ui/window.blp:108
msgid "Toggle Sidebar"
msgstr "Alternar Barra Lateral"

#: src/ui/shortcut_window.blp:102 src/ui/window.blp:59
msgid "Search Notes"
msgstr "Cercar Notes"

#: src/ui/window.blp:121
msgid "Open in Notebook"
msgstr "Obrir al Quadern"

#: src/ui/window.blp:149
msgid "Get started writing"
msgstr "Comença a escribir"

#: src/ui/window.blp:174
msgid "Create a notebook"
msgstr "Crea un quadern"

#: src/ui/window.blp:199
msgid "Trash is empty"
msgstr "La paperera está buida"

#: src/ui/window.blp:212
msgid "_Rename Note"
msgstr "_Reanomenar Nota"

#: src/ui/window.blp:218
msgid "_Export Note"
msgstr "_Exportar Nota"
