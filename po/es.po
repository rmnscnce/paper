msgid ""
msgstr ""
"Project-Id-Version: io.posidon.Paper main\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-23 09:28+0200\n"
"PO-Revision-Date: 2022-05-10 11:56+0200\n"
"Last-Translator: Zagura <zagura@posidon.io>\n"
"Language-Team: spanish <es@li.org>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 41.0\n"

#: data/app.desktop.in:8 src/ui/strings.vala:3
msgid "Paper"
msgstr "Paper"

#: data/app.desktop.in:9
msgid "Notes Manager"
msgstr "Administrador de Notas"

#: data/app.desktop.in:10
msgid "Take notes"
msgstr "Toma notas"

#: data/app.desktop.in:12
msgid "Notebook;Note;Notes;Text;Markdown;Notepad;Write;School;Post-it;Sticky;"
msgstr ""
"Libreta;Nota;Notas;Texto;Markdown;Notepad;Escribe;Cole;Colegio;Post-it;"
"Sticky;"

#: data/app.desktop.in:21 src/ui/strings.vala:8 src/ui/shortcut_window.blp:17
#: src/ui/window.blp:43 src/ui/window.blp:155
msgid "New Note"
msgstr "Nota Nueva"

#: data/app.desktop.in:25 src/ui/strings.vala:14 src/ui/shortcut_window.blp:32
#: src/ui/window.blp:180
msgid "New Notebook"
msgstr "Libreta Nueva"

#: data/app.desktop.in:29 src/ui/strings.vala:39
#: src/ui/edit_view/edit_view.blp:113 src/ui/window.blp:128
msgid "Markdown Cheatsheet"
msgstr "Chuleta de Markdown"

#: data/app.desktop.in:33 src/ui/shortcut_window.blp:57
msgid "Preferences"
msgstr "Preferencias"

#: data/app.gschema.xml:6
msgid "Font"
msgstr "Fuente"

#: data/app.gschema.xml:7 src/ui/preferences.blp:15
msgid "The font notes will be displayed in"
msgstr "La fuente en la que se mostraran las notas"

#: data/app.gschema.xml:11 src/ui/preferences.blp:25
msgid "OLED Mode"
msgstr "Modo OLED"

#: data/app.gschema.xml:15
msgid "Notes Directory"
msgstr "Carpeta de notas"

#: data/app.gschema.xml:16
msgid "Where the notebooks are stored"
msgstr "Donde las notas se almacenan"

#: data/app.metainfo.xml.in:5
msgid "Note-taking app"
msgstr "App para tomar apuntes"

#: data/app.metainfo.xml.in:7
msgid "Create notebooks, take notes in markdown"
msgstr "Crea libretas, toma notas en markdown"

#: data/app.metainfo.xml.in:8
msgid "Features:"
msgstr "Funcionalidades:"

#: data/app.metainfo.xml.in:10
msgid "Almost WYSIWYG markdown rendering"
msgstr "Renderizage casi perfecto de markdown"

#: data/app.metainfo.xml.in:11
msgid "Searchable through GNOME search"
msgstr "Buscable atraves de busqueda de GNOME"

#: data/app.metainfo.xml.in:12
msgid "Highlight &amp; Strikethrough text formatting"
msgstr "Formato tachado y subrayado"

#: data/app.metainfo.xml.in:13
msgid "App recoloring based on notebook color"
msgstr "Recoloración de la interfaz a base del color de la libreta"

#: data/app.metainfo.xml.in:14
msgid "Trash can"
msgstr "Papelera"

#: data/app.metainfo.xml.in:30
msgid "Markdown document"
msgstr "Documento markdown"

#: src/ui/strings.vala:4
msgid "Trash"
msgstr "Papelera"

#: src/ui/strings.vala:5
#, c-format
msgid "%d Notes"
msgstr "%d Notas"

#: src/ui/strings.vala:6
msgid "Are you sure you want to delete everything in the trash?"
msgstr "De verdad quiere borrar todo en la papelera?"

#: src/ui/strings.vala:7 src/ui/window.blp:50
msgid "Empty Trash"
msgstr "Vaciar Papelera"

#: src/ui/strings.vala:9
msgid "Create/choose a notebook before creating a note"
msgstr "Cree/escoga una libreta antes de crear una nota"

#: src/ui/strings.vala:10
msgid "Please, select a note to edit"
msgstr "Seleccione una nota para editar, porfavor"

#: src/ui/strings.vala:11
msgid "Please, select a note to delete"
msgstr "Seleccione una nota para borrar, porfavor"

#: src/ui/strings.vala:12
msgid "Export Note"
msgstr "Exportar Nota"

#: src/ui/strings.vala:13
msgid "Export"
msgstr "Exportar"

#: src/ui/strings.vala:15
msgid "Rename Note"
msgstr "Renombrar Nota"

#: src/ui/strings.vala:16
msgid "Move to Notebook"
msgstr "Mover a Libreta"

#: src/ui/strings.vala:17
msgid "Move"
msgstr "Mover"

#: src/ui/strings.vala:18
#, c-format
msgid "Note “%s” already exists in notebook “%s”"
msgstr "Nota “%s” ya existe en libreta “%s”"

#: src/ui/strings.vala:19
#, c-format
msgid "Are you sure you want to delete the note “%s”?"
msgstr "De verdad quiere borrar la nota “%s”?"

#: src/ui/strings.vala:20
msgid "Delete Note"
msgstr "Borrar Nota"

#: src/ui/strings.vala:21
#, c-format
msgid "Are you sure you want to delete the notebook “%s”?"
msgstr "De verdad quiere borrar la libreta “%s”?"

#: src/ui/strings.vala:22
msgid "Delete Notebook"
msgstr "Borrar Libreta"

#: src/ui/strings.vala:23 src/ui/shortcut_window.blp:37
msgid "Edit Notebook"
msgstr "Editar Libreta"

#: src/ui/strings.vala:24
msgid "Note name shouldn’t contain “.” or “/”"
msgstr "Nombres de notas no deben contener “.” o “/”"

#: src/ui/strings.vala:25
msgid "Note name shouldn’t be blank"
msgstr "Nombres de notas no deben ser vacios"

#: src/ui/strings.vala:26
#, c-format
msgid "Note “%s” already exists"
msgstr "Nota “%s” ya existe"

#: src/ui/strings.vala:27
msgid "Couldn’t create note"
msgstr "Nota no se pudo crear"

#: src/ui/strings.vala:28
msgid "Couldn’t change note"
msgstr "Nota no se pudo cambiar"

#: src/ui/strings.vala:29
msgid "Couldn’t delete note"
msgstr "Nota no se pudo borrar"

#: src/ui/strings.vala:30
msgid "Couldn’t restore note"
msgstr "Nota no se pudo recuperar"

#: src/ui/strings.vala:31
#, c-format
msgid "Saved “%s” to “%s”"
msgstr "“%s” se guardó en “%s”"

#: src/ui/strings.vala:32
msgid "Unknown error"
msgstr "Error desconocido"

#: src/ui/strings.vala:33
msgid "Notebook name shouldn’t contain “.” or “/”"
msgstr "Nombres de libretas no deben contener “.” o “/”"

#: src/ui/strings.vala:34
msgid "Notebook name shouldn’t be blank"
msgstr "Nombres de libretas no deben ser vacios"

#: src/ui/strings.vala:35
#, c-format
msgid "Notebook “%s” already exists"
msgstr "Libreta “%s” ya existe"

#: src/ui/strings.vala:36
msgid "Couldn’t create notebook"
msgstr "Libreta no se pudo crear"

#: src/ui/strings.vala:37
msgid "Couldn’t change notebook"
msgstr "Libreta no se pudo cambiar"

#: src/ui/strings.vala:38
msgid "Couldn’t delete notebook"
msgstr "Libreta no se pudo borrar"

#: src/ui/strings.vala:40
msgid "Search"
msgstr "Buscar"

#: src/ui/strings.vala:41 src/ui/notebook_sidebar/note_menu.blp:36
msgid "Rename"
msgstr "Renombrar"

#: src/ui/strings.vala:42
msgid "Couldn’t find an app to handle file uris"
msgstr "No se encontró app para manejar uris"

#: src/ui/strings.vala:43
msgid "Apply"
msgstr "Aplicar"

#: src/ui/strings.vala:44
#: src/ui/popup/notebook_selection_popup/notebook_selection_popup.blp:34
msgid "Cancel"
msgstr "Cancelar"

#: src/ui/strings.vala:45
msgid "Pick where the notebooks will be stored"
msgstr "Escoja donde las notas se almacenaran"

#: src/ui/strings.vala:46
msgid "All Notes"
msgstr "Todas las Notas"

#: src/ui/edit_view/edit_view.blp:50 src/ui/markdown/heading_popover.blp:48
msgid "Plain Text"
msgstr "Texto Simple"

#: src/ui/edit_view/edit_view.blp:51 src/ui/markdown/heading_popover.blp:12
msgid "Heading 1"
msgstr "Encabezado 1"

#: src/ui/edit_view/edit_view.blp:52 src/ui/markdown/heading_popover.blp:18
msgid "Heading 2"
msgstr "Encabezado 2"

#: src/ui/edit_view/edit_view.blp:53 src/ui/markdown/heading_popover.blp:24
msgid "Heading 3"
msgstr "Encabezado 3"

#: src/ui/edit_view/edit_view.blp:54 src/ui/markdown/heading_popover.blp:30
msgid "Heading 4"
msgstr "Encabezado 4"

#: src/ui/edit_view/edit_view.blp:55 src/ui/markdown/heading_popover.blp:36
msgid "Heading 5"
msgstr "Encabezado 5"

#: src/ui/edit_view/edit_view.blp:56 src/ui/markdown/heading_popover.blp:42
msgid "Heading 6"
msgstr "Encabezado 6"

#: src/ui/edit_view/edit_view.blp:67 src/ui/shortcut_window.blp:67
msgid "Bold"
msgstr "Negrita"

#: src/ui/edit_view/edit_view.blp:73 src/ui/shortcut_window.blp:72
msgid "Italic"
msgstr "Italico"

#: src/ui/edit_view/edit_view.blp:79 src/ui/shortcut_window.blp:77
msgid "Strikethrough"
msgstr "Tachado"

#: src/ui/edit_view/edit_view.blp:85 src/ui/shortcut_window.blp:82
msgid "Highlight"
msgstr "Subrayado"

#: src/ui/edit_view/edit_view.blp:95 src/ui/shortcut_window.blp:87
msgid "Insert Link"
msgstr "Insertar Enlace"

#: src/ui/edit_view/edit_view.blp:101
msgid "Insert Code"
msgstr "Insertar Codigo"

#: src/ui/notebooks_bar/notebook_create_popup.blp:54
msgid "Notebook Name"
msgstr "Nombre de Libreta"

#: src/ui/notebooks_bar/notebook_create_popup.blp:72
msgid "First Characters"
msgstr "Primeros Carácteres"

#: src/ui/notebooks_bar/notebook_create_popup.blp:73
msgid "Initials"
msgstr "Iniciales"

#: src/ui/notebooks_bar/notebook_create_popup.blp:74
msgid "Initials: camelCase"
msgstr "Iniciales: camelCase"

#: src/ui/notebooks_bar/notebook_create_popup.blp:75
msgid "Initials: snake_case"
msgstr "Iniciales: snake_case"

#: src/ui/notebooks_bar/notebook_create_popup.blp:76
msgid "Icon"
msgstr "Icono"

#: src/ui/notebooks_bar/notebook_create_popup.blp:104
msgid "Notebook Color"
msgstr "Color de Libreta"

#: src/ui/notebooks_bar/notebook_create_popup.blp:114
msgid "Create Notebook"
msgstr "Crear Libreta"

#: src/ui/notebooks_bar/notebook_menu.blp:16
msgid "Edit"
msgstr "Editar"

#: src/ui/notebooks_bar/notebook_menu.blp:26
msgid "Move all to Trash"
msgstr "Mover Todo a la Papelera"

#: src/ui/notebooks_bar/notebooks_bar.blp:73
msgid "_New Notebook"
msgstr "_Nueva Libreta"

#: src/ui/notebooks_bar/notebooks_bar.blp:77
msgid "_Edit Notebook"
msgstr "_Editar Libreta"

#: src/ui/notebooks_bar/notebooks_bar.blp:83
msgid "_Preferences"
msgstr "_Preferencias"

#: src/ui/notebooks_bar/notebooks_bar.blp:87
msgid "_Keyboard Shortcuts"
msgstr "_Atajos"

#: src/ui/notebooks_bar/notebooks_bar.blp:91
msgid "_About"
msgstr "_Acerca de Paper"

#: src/ui/notebook_sidebar/note_create_popup.blp:37
msgid "Note Name"
msgstr "Nombre de Nota"

#: src/ui/notebook_sidebar/note_create_popup.blp:46
msgid "Create Note"
msgstr "Crear Nota"

#: src/ui/notebook_sidebar/note_menu.blp:16
msgid "Restore from Trash"
msgstr "Recuperar de la Papelera"

#: src/ui/notebook_sidebar/note_menu.blp:26
msgid "Delete from Trash"
msgstr "Borrar de la Papelera"

#: src/ui/notebook_sidebar/note_menu.blp:46
msgid "Move to Notebook…"
msgstr "Mover a Libreta…"

#: src/ui/notebook_sidebar/note_menu.blp:56
msgid "Move to Trash"
msgstr "Mover a la Papelera"

#: src/ui/notebook_sidebar/note_menu.blp:66
msgid "Open Containing Folder"
msgstr "Ver en Carpeta"

#: src/ui/popup/notebook_selection_popup/notebook_selection_popup.blp:39
msgid "Confirm"
msgstr "Confirmar"

#: src/ui/preferences.blp:14
msgid "Note Font"
msgstr "Fuente de Notas"

#: src/ui/preferences.blp:18
msgid "Pick a font for displaying the notes' content"
msgstr "Escoge una fuente para mostrar el contenido de las notas"

#: src/ui/preferences.blp:26
msgid "Makes the dark theme pitch black"
msgstr "Cambia el tema oscuro a color negro"

#: src/ui/preferences.blp:36
msgid "Notes Storage Location"
msgstr "Almacenage de Notas"

#: src/ui/preferences.blp:37
msgid "Where the notebooks are stored (requires app restart)"
msgstr "Donde las notas se almacenan (requiere reiniciar la app)"

#: src/ui/shortcut_window.blp:14
msgid "Note"
msgstr "Nota"

#: src/ui/shortcut_window.blp:22
msgid "Edit Note"
msgstr "Editar Nota"

#: src/ui/shortcut_window.blp:29
msgid "Notebook"
msgstr "Libreta"

#: src/ui/shortcut_window.blp:44
msgid "General"
msgstr "General"

#: src/ui/shortcut_window.blp:47
msgid "Show Keyboard Shortcuts"
msgstr "Mostrar Atajos de Teclado"

#: src/ui/shortcut_window.blp:52
msgid "Quit"
msgstr "Salir"

#: src/ui/shortcut_window.blp:64
msgid "Formatting"
msgstr "Formato"

#: src/ui/shortcut_window.blp:94
msgid "Navigation"
msgstr "Navegación"

#: src/ui/shortcut_window.blp:97 src/ui/window.blp:108
msgid "Toggle Sidebar"
msgstr "Alternar Barra Lateral"

#: src/ui/shortcut_window.blp:102 src/ui/window.blp:59
msgid "Search Notes"
msgstr "Buscar Notas"

#: src/ui/window.blp:121
msgid "Open in Notebook"
msgstr "Abrir en Libreta"

#: src/ui/window.blp:149
msgid "Get started writing"
msgstr "Empieza a escribir"

#: src/ui/window.blp:174
msgid "Create a notebook"
msgstr "Crea una libreta"

#: src/ui/window.blp:199
msgid "Trash is empty"
msgstr "La papelera está vacia"

#: src/ui/window.blp:212
msgid "_Rename Note"
msgstr "_Renombrar Nota"

#: src/ui/window.blp:218
msgid "_Export Note"
msgstr "_Exportar Nota"
